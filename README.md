# Порядок выполнения заданий
## Задание 1. Развернуть группу ВМ и балансировщик
1. Переименовать файл secret.tfvars.dist в secret.tfvars, заполнить учетными данными для доступа к yandex cloud
2. Инициализация и запуск виртуальной машины
```bash
terraform init
terraform plan
terraform apply--auto-approve
```
Завершение работы виртуальной машины
```bash
terraform destroy --auto-approve
```
3. Узнать ip адреса виртуальных машин
```bash
yc compute instance get yc-auto-instance-0
yc compute instance get yc-auto-instance-1
```
4. IP-адрес балансировщика можно взять из вывода скрипта Terraform:
lb_address = "xxx.xxx.xxx.xxx"