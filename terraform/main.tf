terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}

// Configure the Yandex.Cloud provider
provider "yandex" {
  token     = "${var.token}"
  cloud_id  = "${var.cloud_id}"
  folder_id = "${var.folder_id}"
}

data "yandex_compute_image" "base_image" {
  family = "${var.yc_image_family}"
}

// Create a new instances
resource "yandex_compute_instance" "node" {
  count       = "${var.cluster_size}"
  name        = "yc-auto-instance-${count.index}"
  hostname    = "yc-auto-instance-${count.index}"
  description = "yc-auto-instance-${count.index} of my cluster"
  zone = "${element(var.zones, count.index)}"

  resources {
    cores  = "${var.instance_cores}"
    memory = "${var.instance_memory}"
  }

  boot_disk {
    initialize_params {
      image_id = "${data.yandex_compute_image.base_image.id}"
      type = "network-nvme"
      size = "30"
    }
  }

  network_interface {
    subnet_id = "${element(local.subnet_ids.0,count.index)}"
    nat       = true
  }

  metadata = {
    ssh-keys  = "ubuntu:${file("${var.public_key_path}")}"
    user-data = "${count.index == 0 ? "" : "${file("bootstrap/metadata.yaml")}"}"
  }

  labels = {
    node_id = "${count.index}"
  }
}

locals {
  external_ips = ["${yandex_compute_instance.node.*.network_interface.0.nat_ip_address}"]
  internal_ips = ["${yandex_compute_instance.node.*.network_interface.0.ip_address}"]
}

// Создайем целевую группу
resource "yandex_lb_target_group" "target-group" {
  name      = "target-group"

  # target {
  #   subnet_id = [for foo in var.cluster_size : "${local.subnet_ids.0[foo]}"]
  #   address   = [for foo in var.cluster_size : "${yandex_compute_instance.node[foo].network_interface.0.ip_address}"]
  # }

  target {
    subnet_id = "${local.subnet_ids.0[0]}"
    address   = "${yandex_compute_instance.node.0.network_interface.0.ip_address}"
  }

  target {
    subnet_id = "${local.subnet_ids.0[1]}"
    address   = "${yandex_compute_instance.node.1.network_interface.0.ip_address}"
  }

  target {
    subnet_id = "${local.subnet_ids.0[2]}"
    address   = "${yandex_compute_instance.node.2.network_interface.0.ip_address}"
  }

}