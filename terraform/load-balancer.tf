resource "yandex_lb_network_load_balancer" "todo_lb" {
  name = "todo-lb"

  listener {
    name = "todo-listener"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = "${yandex_lb_target_group.target-group.id}"

    healthcheck {
      name = "todo-http-hc"
      http_options {
        port = 80
        path = "/"
      }
    }
  }
}