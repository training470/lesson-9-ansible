output "external_ip_addresses" {
  value = "${local.external_ips}"
}

output "internal_ip_addresses" {
  value = "${local.internal_ips}"
}

output "subnet_ids" {
  value = "${local.subnet_ids}"
}

output "folder_id" {
  value = "${var.folder_id}"
}

# locals {
#   lbaddress = tolist(tolist(yandex_lb_network_load_balancer.todo_lb.listener).0.external_address_spec).0.address
#   lbport    = tolist(yandex_lb_network_load_balancer.todo_lb.listener).0.port
# }

# output "lb_address" {
#   value = "${local.lbaddress}:${local.lbport}"
# }